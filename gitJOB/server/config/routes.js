/**
 * Routes for express app
 */
import passport from 'passport';
import unsupportedMessage from '../db/unsupportedMessage';
import { controllers, passport as passportConfig } from '../db';

const usersController = controllers && controllers.users;
const jobsController = controllers && controllers.jobs;
const appController = controllers && controllers.applications
const companyController = controllers && controllers.company

export default (app) => {
  // user routes
  if (usersController) {
    app.post('/login', usersController.login);
    app.post('/signup', usersController.signUp);
    app.post('/logout', usersController.logout);
  } else {
    console.warn(unsupportedMessage('users routes'));
  }

  if (passportConfig && passportConfig.google) {
    // google auth
    // Redirect the user to Google for authentication. When complete, Google
    // will redirect the user back to the application at
    // /auth/google/return
    
    // Authentication with google requires an additional scope param, for more info go
    // here https://developers.google.com/identity/protocols/OpenIDConnect#scope-param
    app.get('/auth/google', passport.authenticate('google', {
      scope: [
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/userinfo.email'
      ]
    }));

    // Google will redirect the user to this URL after authentication. Finish the
    // process by verifying the assertion. If valid, the user will be logged in.
    // Otherwise, the authentication has failed.
    app.get('/auth/google/callback',
      passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/login'
      })
    );
  }
  // job routes
  if (jobsController) {
    app.get('/posting', jobsController.all);
    app.post('/posting/:id', jobsController.addQueue);
    app.post('/posting/nope/:id', jobsController.addNope);
    app.delete('/posting/:id', jobsController.remove);
  } else {
    console.warn(unsupportedMessage('job routes'));
  }

  //application routes
  if (appController) {
    app.get('/apps', appController.all)
    app.post('/apps/:id', appController.moveUp);
  } else {
    console.warn(unsupportedMessage('app routes'))
  }

  if (companyController) {
    app.get('/company/:id', companyController.getContacts)
    app.post('/company/:id', companyController.addContact)
  }
};

