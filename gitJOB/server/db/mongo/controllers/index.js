import jobs from './jobs';
import users from './users'
import applications from './applications';
import company from './company';

export { jobs, users, applications };

export default {
  jobs,
  users,
  applications,
  company
};
